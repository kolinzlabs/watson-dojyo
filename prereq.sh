#!/bin/bash

# Update package lists
echo "# Updating package lists"
apt-get update -y
apt-get install -y wget unzip nano software-properties-common curl
apt-add-repository -y ppa:git-core/ppa
apt-get update -y

# Set Timezone
timedatectl set-timezone Asia/Tokyo

# Install Git
echo "# Installing Git"
apt-get install -y git

# node.js & npm install
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs

# Install Node-RED
npm install -g --unsafe-perm node-red
npm install -g pm2

# Install IBM Cloud Developer Tool
curl -sL https://ibm.biz/idt-installer | bash

# Install IBM Cloud CLI
wget https://public.dhe.ibm.com/cloud/bluemix/cli/bluemix-cli/0.12.1/IBM_Cloud_CLI_0.12.1_amd64.tar.gz
tar -xf IBM_Cloud_CLI_0.12.1_amd64.tar.gz
cd Bluemix_CLI
./install_bluemix_cli

# Print installation details for user
echo ''
echo 'Installation completed, versions installed are:'
echo ''
echo -n 'Node:'
node --version
echo -n 'npm:'
npm --version
echo -n 'IBM Cloud CLI version:'
bx --version
echo -n 'IBM Cloud Developer Tool help:'
ibmcloud dev help